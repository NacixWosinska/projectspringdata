package edu.ib.projektspring.controller;
import edu.ib.projektspring.entity.Order;
import edu.ib.projektspring.entity.Product;
import edu.ib.projektspring.service.OrderService;
import edu.ib.projektspring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/api/product/all")
    public Iterable<Product> getAllProducts(){
        return productService.findAll();
    }

    @GetMapping("/api/product")
    public Optional<Product> getProductById(@RequestParam Long id) {
        return productService.findById(id);
    }

    @PostMapping("/api/admin/product")
    public Product addProduct(@RequestBody Product product) {
        return productService.add(product);
    }

    @PutMapping("/api/admin/product")
    public Product modifyProduct(@RequestBody Product product) {
        return productService.add(product);
    }

    @PatchMapping("/api/admin/product")
    public void patchProduct(
            @RequestParam Long id,
            @RequestBody Product product) {

        Product product1 = productService.findById(id).orElseThrow(NoSuchElementException::new);
        boolean needUpdate = false;

        if (StringUtils.hasLength(product.getName())) {
            product1.setName(product.getName());
            needUpdate = true;
        }

        if (StringUtils.hasLength(String.valueOf(product.getPrice()))) {
            product1.setPrice(product.getPrice());
            needUpdate = true;
        }
        if (needUpdate) {
            productService.add(product1);
        }
    }
}