FROM openjdk:8-jdk-alpine
ADD target/projektspring-0.0.1-SNAPSHOT.jar .
EXPOSE 8000
CMD java -jar projektspring-0.0.1-SNAPSHOT.jar