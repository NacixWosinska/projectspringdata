package edu.ib.projektspring;

import edu.ib.projektspring.entity.User;
import edu.ib.projektspring.entity.UserDto;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserDtoBuilder {
    private String name;
    private String hashPassword;
    private String role;
    private User user;

    public UserDtoBuilder(User user) {
        this.user = user;
        this.name = user.getName();
        this.role = user.getRole();
        this.hashPassword = getUserDto().getPasswordHash();
    }

    public String getEncodedPassword() {
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        String password = user.getPassword();
        PasswordEncoder passwordEncoder = passwordEncoderConfig.passwordEncoder();
        hashPassword = passwordEncoder.encode(password);
        return hashPassword;
    }

    public UserDto getUserDto(){
        return new UserDto(name, getEncodedPassword(), role);
    }



}
