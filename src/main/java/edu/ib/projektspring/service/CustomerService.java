package edu.ib.projektspring.service;

import edu.ib.projektspring.entity.Customer;
import edu.ib.projektspring.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepo customerRepo;

    @Autowired
    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public Optional<Customer> findById(Long id){
        return customerRepo.findById(id);
    }

    public Iterable<Customer> findAll(){
        return customerRepo.findAll();
    }

    public Customer add(Customer customer){
        return customerRepo.save(customer);
    }


}
