package edu.ib.projektspring.controller;
import edu.ib.projektspring.entity.Order;
import edu.ib.projektspring.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("api/order/all")
    public Iterable<Order> getAllOrders(){
        return orderService.findAll();
    }

    @GetMapping("api/order")
    public Optional<Order> getOrderById(@RequestParam Long id) {
        return orderService.findById(id);
    }

    @PostMapping("/api/order")
    public Order addOrder(@RequestBody Order order) {
        return orderService.add(order);
    }

    @PutMapping("/api/admin/order")
    public Order modifyOrder(@RequestBody Order order) {
        return orderService.add(order);
    }

    @PatchMapping("/api/admin/order")
    public void patchOrder(
            @RequestParam Long id,
            @RequestBody Order order) {

        Order order1 = orderService.findById(id).orElseThrow(NoSuchElementException::new);
        boolean needUpdate = false;

        if (StringUtils.hasLength(order.getStatus())) {
            order1.setStatus(order.getStatus());
            needUpdate = true;
        }

        if (StringUtils.hasLength(String.valueOf(order.getPlaceDate()))) {
            order1.setPlaceDate(order.getPlaceDate());
            needUpdate = true;
        }
        if (StringUtils.hasLength(String.valueOf(order.getCustomer()))) {
            order1.setCustomer(order.getCustomer());
            needUpdate = true;
        }
        if (StringUtils.hasLength(String.valueOf(order.getProducts()))) {
            order1.setProducts(order.getProducts());
            needUpdate = true;
        }
        if (needUpdate) {
            orderService.add(order1);
        }
    }
}