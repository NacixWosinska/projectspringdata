package edu.ib.projektspring.service;

import edu.ib.projektspring.entity.Product;
import edu.ib.projektspring.entity.User;
import edu.ib.projektspring.entity.UserDto;
import edu.ib.projektspring.repository.ProductRepo;
import edu.ib.projektspring.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public UserDto findByName(String name){
        return userRepo.getUserDtoByName(name);
    }

    public UserDto save(UserDto user){
        return userRepo.save(user);
    }
}
