package edu.ib.projektspring.repository;
import edu.ib.projektspring.entity.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<UserDto, Long> {

    UserDto getUserDtoByName(String name);
}
