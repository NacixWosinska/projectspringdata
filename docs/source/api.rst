.. toctree::
    :maxdepth: 2  ".. głębokość sekcji w generowanym spisie"
    :caption: Zawartość: ".. użyty tytuł"

    getting_started

REST API - opis
======================
Endpointy, które odnoszą się do products:
--------------------------------------------
- GET …/api/product - zwraca produkt o odpowiednim id,

- GET …/api/product/all - zwraca listę wszystkich produktów,

- POST …/api/admin/product - umieszcza nowy produkt w bazie,

- PUT …/api/admin/product - modufikuje istniejący produkt o zadanym id,

- PATCH …/api/admin/product - częściowo modyfikuje istniejący produkt o zadanym id,

Endpointy, które odnoszą się do customers:
---------------------------------------------
- GET …/api/customer - zwraca klienta o podanym id,

- GET …/api/customer/all - zwraza listę wszystkich klientów,

- POST …/api/admin/customer - umieszcza nowego klienta w bazie,

- PUT …/api/admin/customer - modufikuje istniejącego klienta o zadanym id,

- PATCH …/api/admin/customer - częściowo modyfikuje istniejącego klienta o podanym id,

Endpointy, które odnoszą się do orders:
--------------------------------------------
- GET …/api/order - zwraca zamówienie o podanym id,

- GET ../api/order/all - zwraza listę wszystkich zamówień,

- POST …/api/order - umieszcza nowe zamówienie w bazie,

- PUT …/api/admin/order - modufikuje istniejące zamówienie o zadanym id,

- PATCH …/api/admin/order - częściowo modyfikuje istniejące zamówienie o podanym id.