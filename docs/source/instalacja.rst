.. toctree::
    :maxdepth: 2  ".. głębokość sekcji w generowanym spisie"
    :caption: Zawartość: ".. użyty tytuł"

    getting_started
============================
Instrukcja docker-compose
============================
Co należy zainstalować?
----------------------------
Docker oraz docker-compose, a także stworzyć nowy projekt Spring Web w gradle.


Konfiguracja krok po kroku
----------------------------

- Najpierw zdefiniuj plik *Dockerfile* (koniecznie bez rozszerzenia!). Umieść go w forlderze nadrzędnym projektu. Jego zawartość:

.. code-block:: java

   FROM gradle:6.0.1-jdk8 AS build
   USER root
   RUN mkdir app
   COPY src/ /app/src/
   COPY build.gradle /app/
   COPY settings.gradle /app/
   WORKDIR /app
   RUN gradle bootJar
   FROM openjdk:8-jdk-alpine
   COPY --from=build /app/build/libs/app.jar app.jar
   ENTRYPOINT ["java","-jar","/app.jar"]


- Przygotuj skrypt inicjalizujący twoją bazę danych o rozszerzeniu *.sh* - wszystko umieść w katalogu docker-entrypoint-initdb.d.

- Potem, dodaj plik *docker-compose.yaml*. Pamiętaj, że w sekcji "ports" pierwszy port oznacza ten, na którym słuchany jest środowisko kontenerowe, natomiast drugi to nasz lokalny port:

.. code-block:: java

  version: "3"
  services:
      app:
          image: "nazwa_obrazu"
          build:
              context: .
              dockerfile: "Dockerfile"
          environment:
              POSTGRES_USER: ${POSTGRES_USER}
              POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
          ports:
              - 8000:8080
      db:
          image: postgres:latest
          volumes:
              - "./docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d"
          environment:
              POSTGRES_USER: ${POSTGRES_USER}
              POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
          ports:
              - ${DB_PORT}:5432

- A następnie plik *.env*:

.. code-block:: java

  POSTGRES_USER=postgres
  POSTGRES_PASSWORD=mysecretpass
  DB_PORT=5433

- Pamiętaj, aby odpowiednio zmienić port i resztę konfiguracji w *application.properties*:

.. code-block:: java

  server.port=8080
  spring.datasource.url=jdbc:postgresql://db:5432/docker
  spring.datasource.username=${POSTGRES_USER}
  spring.datasource.password=${POSTGRES_PASSWORD}
  spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect
  spring.jpa.hibernate.ddl-auto = update

- Zbuduj obraz komendą:

``docker-compose build``

- A następne uruchom kontenery bazy danych i aplikacji:

``docker-compose up``

