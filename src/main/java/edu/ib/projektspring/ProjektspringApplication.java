package edu.ib.projektspring;

import edu.ib.projektspring.entity.User;
import edu.ib.projektspring.entity.UserDto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjektspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjektspringApplication.class, args);

        UserDtoBuilder userDtoBuilder = new UserDtoBuilder(new User("admin","adminek", "ROLE_ADMIN"));
        UserDto userDto = userDtoBuilder.getUserDto();
        System.out.println(userDto);
    }

}
