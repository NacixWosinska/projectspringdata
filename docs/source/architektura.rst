Architektura projektu
============================
Diagram przypadków użycia
----------------------------

.. uml::

	@startuml
	left to right direction
	actor "ROLE_CUSTOMER" as customer

    	usecase (Pobiera listę wszystkich zamówień) as (1)
	usecase (Pobiera zamówienie o danym id) as (2)
	usecase (Pobiera produkt o danym id) as (3)

	customer --> 1
	customer --> 2
	customer --> 3
	@enduml

Diagram klas
--------------------------

.. uml::

   @startuml
   skinparam classAttributeIconSize 0
	class Order{
	{field}-id:Long
	{field}-customer:Customer
	{field}-products:Set<Product>
	{field}-placeDate:LocalDateTime
	{field}-status:String

	{method}+getters
	{method}+setters
	}

	class Product{
	{field}-id:Long
	{field}-name:String
	{field}-price:float
	{field}-available:boolean

	{method}+getters
	{method}+setters
	}

	class Customer{
	{field}-id:Long
	{field}-name:String
	{field}-address:String

	{method}+getters
	{method}+setters
	}

	interface ProductRepo
   	interface OrderRepo
   	interface CustomerRepo

	class ProductManager{
            {field}-ProductRepo:ProductRepo

            {method}+findAllById
            {method}+findAll
            {method}+save
            {method}+saveFromPatch
            {method}+fillDB
            }

   class OrderManager{
               {field}-OrderRepo:OrderRepo

               {method}+findAllById
               {method}+findAll
               {method}+save
               {method}+saveFromPatch
               {method}+fillDB
               }

   class CustomerManager{
               {field}-CustomerRepo:CustomerRepo

               {method}+findAllById
               {method}+findAll
               {method}+save
               {method}+saveFromPatch
               {method}+fillDB
               }

   class ProductApi{
                  {field}-products:ProductManager

                  {method}+getAll
                  {method}+getById
                  {method}+addProduct
                  {method}+updateProduct
                  {method}+patchProduct
                  }

   class OrderApi{
                     {field}-orders:OrderManager

                     {method}+getByIdOrders
                     {method}+getAllOrders
                     {method}+addOrder
                     {method}+updateOrder
                     {method}+patchOrder
                     }

   class CustomerApi{
                     {field}-customers:CustomerManager

                     {method}+getAllCustomers
                     {method}+getByIdCustomer
                     {method}+addCustomer
                     {method}+updateCustomer
                     {method}+patchCustomer
                     }

   ProductApi --> ProductManager
   OrderApi --> OrderManager
   CustomerApi --> CustomerManager

   ProductManager --> ProductRepo
   OrderManager --> OrderRepo
   CustomerManager --> CustomerRepo

   ProductRepo --> Product
   OrderRepo --> Order
   CustomerRepo --> Customer

   Product *-- Order
   Order -- Customer




   @enduml