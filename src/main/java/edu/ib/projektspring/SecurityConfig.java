package edu.ib.projektspring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        PasswordEncoder passwordEncoder = passwordEncoderConfig.passwordEncoder();
        auth.jdbcAuthentication()
                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM user_dto u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM user_dto u WHERE u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
        System.out.println(userDetailsService());
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/api/product").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET,"/api/product/all").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET,"/api/order").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET,"/api/order/all").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.POST,"/api/order").hasAnyRole("CUSTOMER", "ADMIN")

                .antMatchers(HttpMethod.GET,"/api/customer").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.GET,"/api/customer/all").hasRole("CUSTOMER")

                .antMatchers(HttpMethod.POST,"/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/order").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/order")
                .hasRole("ADMIN")
                .and()
                .httpBasic().and().csrf().disable().headers().frameOptions().disable();

    }
}
