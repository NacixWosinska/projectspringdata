======================================
Czym jest Docker oraz docker-compose?
========================================
Docker
-------
Docker to narzędzie umożliwiające stworzenie wirtualnego środowiska - konteneru. Może więc
uruchomić aplikację w odseparowanym systemie operacyjnym. Kontenery są uruchamiane na podstawie obrazów - jest w nich zawarty kod aplikacji, zdefiniowane np. jdk projektu oraz wszelkie zaimportowane dependencies.

docker-compose
---------------

Pozwala na uruchamianie wielu kontenerów (zależnych od siebie) jednocześnie. Np. można całkowicie oddzielnie uruchomić kontener z bazą danych oraz kontener zawierający aplikację. Umożliwia także zarządzanie czynnościami, ustalaniem z góry kolejności ich uruchamiania.
