.. lab-documentation documentation master file, created by
   sphinx-quickstart on Tue May 24 13:21:19 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lab-documentation's documentation!
=============================================

.. toctree::
    :maxdepth: 2  ".. głębokość sekcji w generowanym spisie"
    :caption: Zawartość: ".. użyty tytuł"

    getting_started


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
