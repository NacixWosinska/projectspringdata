package edu.ib.projektspring;

import edu.ib.projektspring.entity.*;
import edu.ib.projektspring.repository.CustomerRepo;
import edu.ib.projektspring.repository.OrderRepo;
import edu.ib.projektspring.repository.ProductRepo;
import edu.ib.projektspring.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepo orderRepository;
    private CustomerRepo customerRepository;
    private UserRepo userRepository;
    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepo orderRepository, CustomerRepo customerRepository
    , UserRepo userRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Dach", 1000, false);
        Product product1 = new Product("Taczka", 35, true);
        Customer customer = new Customer("Natalia Wosińska", "Wieluń");
        Customer customer2 = new Customer("Patrycja Woszczyk", "Wrocław");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }};
        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");
        Order order2 = new Order(customer2, Collections.singleton(product1), LocalDateTime.now(), "not sent");

        User user1 = new User("user1", "user1","ROLE_CUSTOMER");
        User user2 = new User("user2", "user2","ROLE_ADMIN");

        UserDtoBuilder userDtoBuilder1 = new UserDtoBuilder(user1);
        UserDto userDto1 = userDtoBuilder1.getUserDto();
        UserDtoBuilder userDtoBuilder2 = new UserDtoBuilder(user2);
        UserDto userDto2 = userDtoBuilder2.getUserDto();

        productRepository.save(product);
        productRepository.save(product1);
        customerRepository.save(customer);
        customerRepository.save(customer2);
        orderRepository.save(order);
        orderRepository.save(order2);
        userRepository.save(userDto1);
        userRepository.save(userDto2);
    }
}