package edu.ib.projektspring.controller;
import edu.ib.projektspring.entity.Customer;
import edu.ib.projektspring.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("api/customer/all")
    public Iterable<Customer> getAllCustomers(){
        return customerService.findAll();
    }

    @GetMapping("api/customer")
    public Optional<Customer> getCustomerById(@RequestParam Long id) {
        return customerService.findById(id);
    }

    @PostMapping("/api/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerService.add(customer);
    }

    @PutMapping("/api/admin/customer")
    public Customer modifyCustomer(@RequestBody Customer customer) {
        return customerService.add(customer);
    }

    @PatchMapping("/api/admin/customer")
    public void patchCustomer(
            @RequestParam Long id,
            @RequestBody Customer customer) {

        Customer customer1 = customerService.findById(id).orElseThrow(NoSuchElementException::new);
        boolean needUpdate = false;

        if (StringUtils.hasLength(customer.getName())) {
            customer1.setName(customer.getName());
            needUpdate = true;
        }

        if (StringUtils.hasLength(customer.getAddress())) {
            customer1.setAddress(customer.getAddress());
            needUpdate = true;
        }
        if (needUpdate) {
            customerService.add(customer1);
        }
    }
}
